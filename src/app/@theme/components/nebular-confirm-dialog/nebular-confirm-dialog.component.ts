import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-nebular-confirm-dialog',
  templateUrl: './nebular-confirm-dialog.component.html',
  styleUrls: ['./nebular-confirm-dialog.component.scss']
})
export class NebularConfirmDialogComponent implements OnInit {
  title = '';
  message = '';
  constructor(protected dialogRef: NbDialogRef<any>) { }

  ngOnInit(): void {
  }

  onConfirm(): void {
    this.dialogRef.close(true);
  }

  onDismiss(): void {
    this.dialogRef.close(false);
  }
}
