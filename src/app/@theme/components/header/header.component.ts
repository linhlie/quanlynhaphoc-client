import { Component, OnInit } from '@angular/core';
import { NbMenuItem, NbMenuService, NbSidebarService, NbToastrService } from '@nebular/theme';
import { AuthService } from '../../../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userPictureOnly = false;
  user: any = {
    username: '',
  };

  userMenu: NbMenuItem[] = [
    { title: 'Hồ sơ', icon: 'person-outline', data: 'profile' },
    { title: 'Đăng xuất', icon: 'log-out-outline', data: 'log-out' }
  ];

  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private auth: AuthService,
    private router: Router,
    private toastr: NbToastrService) {
  }

  ngOnInit(): void {
    this.user.username = this.auth.getPayload().username;
    this.menuService.onItemClick().subscribe(item => {
      switch (item.item?.data) {
        case 'log-out':
          this.auth.logout().subscribe(res => {
            if (res) {
              this.router.navigate(['/auth/login']);
            } else {
              this.toastr.warning('Logout failed', 'Warning');
            }
          });
          break;
      }
    });
  }

  navigateHome(): void {
    this.menuService.navigateHome();
  }

  navigateChat(): void {
    console.log("object");
    this.router.navigate(['/pages/room']);
  }

  toggleSidebar(): void {
    this.sidebarService.toggle(true);
  }
}
