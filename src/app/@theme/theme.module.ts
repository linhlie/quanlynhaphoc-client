import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { NbActionsModule, NbButtonModule, NbCardModule, NbContextMenuModule, NbIconModule, NbUserModule, NbDialogModule, NbSelectModule } from '@nebular/theme';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { NebularConfirmDialogComponent } from './components/nebular-confirm-dialog/nebular-confirm-dialog.component';



@NgModule({
  declarations: [
    HeaderComponent,
    NotFoundComponent,
    ConfirmationDialogComponent,
    NebularConfirmDialogComponent,
  ],
  imports: [
    CommonModule,
    NbIconModule,
    NbActionsModule,
    NbUserModule,
    NbContextMenuModule,
    NbCardModule,
    NbButtonModule,
    MatDialogModule,
    MatButtonModule,
    NbDialogModule,
    NbCardModule,
    NbSelectModule
  ],
  exports: [
    HeaderComponent,
    NotFoundComponent,
  ]
})
export class ThemeModule {
}
