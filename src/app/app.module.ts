/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import {
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbThemeModule,
  NbLayoutModule,
  NbTimepickerModule,
  NbTreeGridModule,
  NbFormFieldModule,
  NbCardModule,
  NbButtonGroupModule,
  NbButtonModule,
  NbSelectModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import {NbAuthJWTToken, NbAuthModule, NbPasswordAuthStrategy} from '@nebular/auth';
import {environment} from '../environments/environment';
import { ColorPickerModule } from 'ngx-color-picker';
import { CookieService } from 'ngx-cookie-service';
import { AuthInterceptor } from './auth/auth.interceptor';
import { SeoService } from './@core/utils';
import { CommonModule } from '@angular/common';
import { ClassDetailComponent } from './pages/class-detail/class-detail.component';  
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuestionCreateComponent } from './pages/question-create/question-create.component';
import { NotificationComponent } from './pages/notification/notification.component';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent,
    ClassDetailComponent,
    QuestionCreateComponent,
    // NotificationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbEvaIconsModule,
    NbMenuModule.forRoot(),
    NbSidebarModule.forRoot(),
    HttpClientModule,
    NbToastrModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbTimepickerModule.forRoot(),
    NbTreeGridModule,
    ColorPickerModule,
    NbFormFieldModule,
    NbLayoutModule,
    CommonModule,
    NbCardModule,
    FormsModule,
    ReactiveFormsModule,
    NbButtonGroupModule,
    NbButtonModule,
    NbSelectModule,
    MatSidenavModule,
    MatButtonModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
    CookieService,
    SeoService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
