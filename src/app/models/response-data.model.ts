export interface ResponseDataModel {
  data: any;
  listData: any;
  message: string;
  statusCode: string;
  totalRow: number;
  version: string;
}

export interface Result {
  amount: number;
  type: string;
}
