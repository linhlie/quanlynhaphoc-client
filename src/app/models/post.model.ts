export class Post {
  avatar: string;
  content: string;
  created_at: string;
  created_by: string;
  description: string;
  id: number;
  id_image: number
  name: string;
  status: number;
  type: number;
  updated_at: string;
  url: string;
}
