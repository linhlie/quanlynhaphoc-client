export interface Question {
  id: number;
  question:string;
  type:number;
  answer:string;
  status:number;
}

export interface QuestionSubmit {
  question:string;
  type:number;
  answer:string;
  status:number;
}



