export interface Student {
  id: number;
  name: string;
  code: string;
  password: string;
  province_id:number;
  district_id:number;
  ward_id:number;
  full_name: string;
  address: string;
  phone: string;
  status: number;
  point: number;
  email: string;
  class_id:number;
  avatar: string;
  industry_id: number;
  gender: string;
  description: string;
  id_card: string;
  home_town: string;
  birth_place: string;
  date_of_birth: Date;
  society_id: number;
  exam_id: number;
  index: number;
}
