export interface ClassModel {
  code: string;
  created_at: string;
  deleted_at: string;
  id: number;
  id_lecturer: number;
  industry_name: string;
  lecturer_name: string;
  name: string;
  total_student: number;
  updated_at: string;
  industry_id: string;
}

export interface ClassModelSubmit {
  name: string;
  code: string;
  total_student: number;
  id_lecturer: number;
  industry_id: string;
  lecturer_name: string;
}