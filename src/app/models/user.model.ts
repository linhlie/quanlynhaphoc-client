export interface User {
  id: number;
  name: string;
  username:string;
  password: string;
  email: string;
  avatar: string;
  address: string;
  birthDate: string;
  gender: string;
  phone: string;
  status: number;
  role_id: number;
}

export interface UserSubmit {
  name: string;
  username:string;
  password: string;
  email: string;
  avatar: string;
  address: string;
  birthDate: Date;
  gender: string;
  phone: string;
  status: number;
  role_id: number;
}

export interface UserChangePassword {
  id: number;
  newPassword: string;
  oldPassword: string;

  username: string;
}
