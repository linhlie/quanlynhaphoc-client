export class Paper {
  id: number;
  name: string;
  status: number;
}

export class PaperResponse {
  data: Paper[];
  message: string;
  status: number;
}
