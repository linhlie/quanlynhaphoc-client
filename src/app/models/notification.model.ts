export interface Notification {
  id: number;
  content:string;
  type:number;
  id_student:number;
  status:number;
  start_date:Date;
  end_date:Date;
}

export interface NotificationSubmit {

  content:string;
  type:number;
  id_student:number;
  status:number;
  start_date:Date;
  end_date:Date;
}



