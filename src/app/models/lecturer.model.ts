export interface Lecturer {
  id: number;
  name: string;
  email: string;
  address: string;
  gender: string;
  phone: string;
  status: number;
  code:string,
  id_faculty: string,
  date_of_birth: string,
  rank: string,
  deleted_at: string,
}

export interface LecturerSubmit {
  id: number;
  name: string;
  email: string;
  address: string;
  gender: string;
  phone: string;
  status: number;
  code:string,
  id_faculty: string,
  date_of_birth: Date,
  rank: string,
  deleted_at: Date,
}




