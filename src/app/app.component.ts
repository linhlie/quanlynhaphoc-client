/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';

import { SeoService } from './@core/utils/seo.service';
import firebase from "firebase";
import "firebase/firestore";

const config = {
  apiKey: 'AIzaSyB0PR_P6nUWUlxBNrLJqNvRbkeqzEq9gyM',
  databaseURL: 'https://quanlynhaphoc-default-rtdb.firebaseio.com'
};

@Component({
  selector: 'ngx-app',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  constructor(private seoService: SeoService) {
    firebase.initializeApp(config);
  }
  

  ngOnInit(): void {
    this.seoService.trackCanonicalChanges();
  }
}
