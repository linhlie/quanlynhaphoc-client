import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RestResponse} from '../dto/rest-response';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class StatisticService {

  constructor(private http: HttpClient) { }
  statOverview(): Observable<any> {
    return this.http
      .get<RestResponse<any>>(`${environment.apiUrl}/admin/statistic/overview`)
      .pipe(map(res => res.data));
  }

}
