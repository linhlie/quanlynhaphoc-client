import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Student} from '../models/student.model';
import {PageResponse} from '../dto/page-response';
import {environment} from '../../environments/environment';
import {RestResponse} from '../dto/rest-response';
import {map} from 'rxjs/operators';
import { Statistic } from '../models/statistic.model';
import { tap } from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';

@Injectable({
  providedIn: 'root',
})
export class StudentService {

  constructor(private http: HttpClient, private toastr: NbToastrService) { }

  list(page: number = 1, limit: number = 5): Observable<PageResponse<Student[]>> {
    const params = new HttpParams()
      .set('limit', limit.toString())
      .set('page', page.toString());
    return this.http.get<PageResponse<Student[]>>(`${environment.apiUrl}/admin/students`, {params: params});
  }

  listStatus(page: number = 1, limit: number = 5, status: string = ''): Observable<PageResponse<Student[]>> {
    const params = new HttpParams()
      .set('status', status)
      .set('limit', limit.toString())
      .set('page', page.toString());
    return this.http.get<PageResponse<Student[]>>(`${environment.apiUrl}/admin/students`, {params: params});
  }

  createCode(dataSubmit: any ): Observable<Student[]> {
    return this.http
      .post<RestResponse<Student[]>>(`${environment.apiUrl}/admin/students/create-code`, dataSubmit)
      .pipe(
        tap((res: any) => {
          console.log(res.status)
          if (res.status) {
            this.toastr.success('Tạo mã sinh viên thành công', 'Success');
          }
  
          else {
            this.toastr.danger('Có lỗi xảy ra', 'Error');
          }
        }, err => {
          this.toastr.danger('Có lỗi xảy ra', 'Error');
        }),
      );
  }

  statistics(): Observable<Statistic> {
    return this.http.get<Statistic>(`${environment.apiUrl}/admin/students/statistic`);
  }
  
  one(id: number): Observable<Student> {
    return this.http
      .get<RestResponse<Student>>(`${environment.apiUrl}/admin/students/${id}`)
      .pipe(
        map(rs => rs.data),
      );
  }

  activeUser(id: number): Observable<Student> {
    return this.http
      .put<RestResponse<Student>>(`${environment.apiUrl}/user/active/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }

  inactiveUser(id: number): Observable<Student> {
    return this.http
      .put<RestResponse<Student>>(`${environment.apiUrl}/user/inactive/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }

  insertStudent(fileToUpload: File): Observable<Student> {
    const formData: FormData = new FormData();
    formData.append('select_file', fileToUpload, fileToUpload.name);
    return this.http
      .post<RestResponse<Student>>(`${environment.apiUrl}/excel`, formData)
      .pipe(
        tap((res: any) => {
          if(res.status){
            this.toastr.success('Thêm dữ liệu thành công', 'Success');
          } else {
            this.toastr.danger(res.message, 'Error');
          }
          
        }, err => {
          this.toastr.danger('Có lỗi xảy ra', 'Error');
        }),
      );
  }
}
