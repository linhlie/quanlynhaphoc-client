import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Post} from '../models/post.model';
import {PageResponse} from '../dto/page-response';
import {environment} from '../../environments/environment';
import {RestResponse} from '../dto/rest-response';
import {map, toArray} from 'rxjs/operators';
import {Paper} from '../models/paper.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ResponseDataModel } from '../models/response-data.model';
import { NbToastrService } from '@nebular/theme';
import { Result } from '../models/response-data.model';
import {PaperResponse} from '../models/paper.model';
import { Lecturer, LecturerSubmit } from '../models/lecturer.model';
import { tap } from 'rxjs/operators';
import { Question, QuestionSubmit } from '../models/question.model';
import { Notification, NotificationSubmit } from '../models/notification.model';
export class Body{
  code : string;
  papers: string;
}

@Injectable({
  providedIn: 'root',
})
export class HomeService {

  code = '';

  constructor(private http: HttpClient, private toastr: NbToastrService) { }

  // classList(){
  //   return this.http.get<any>(`${environment.apiUrl}/paper`);
  // }
  listLecturer(page: number = 1, limit: number = 5): Observable<PageResponse<Lecturer[]>> {
    const params = new HttpParams()
      .set('limit', limit.toString())
      .set('page', page.toString());
    return this.http.get<PageResponse<Lecturer[]>>(`${environment.apiUrl}/admin/lecturer`, {params: params});
  }

  lecturer(id:number): Observable<RestResponse<Lecturer>> {
    return this.http.get<RestResponse<Lecturer>>(`${environment.apiUrl}/admin/lecturer/${id}`);
  }

  listQuestion(page: number = 1, limit: number = 5): Observable<PageResponse<Question[]>> {
    const params = new HttpParams()
      .set('limit', limit.toString())
      .set('page', page.toString());
    return this.http.get<PageResponse<Question[]>>(`${environment.apiUrl}/admin/questions`, {params: params});
  }


  question(id:number): Observable<RestResponse<Question>> {
    return this.http.get<RestResponse<Question>>(`${environment.apiUrl}/admin/questions/${id}`);
  }

  notification(id:number): Observable<RestResponse<Notification>> {
    return this.http.get<RestResponse<Notification>>(`${environment.apiUrl}/admin/notification/${id}`);
  }


  listNotifications(page: number = 1, limit: number = 5): Observable<PageResponse<Notification[]>> {
    const params = new HttpParams()
      .set('limit', limit.toString())
      .set('page', page.toString());
    return this.http.get<PageResponse<Notification[]>>(`${environment.apiUrl}/admin/notification`, {params: params});
  }

  status(): Observable<PageResponse<any>> {
    return this.http.get<any>(`${environment.apiUrl}/admin/status`);
  }

  updateStatusStudent(status: any): Observable<ResponseDataModel> {
    return this.http.post(`${environment.apiUrl}/admin/students/update-status`, status).pipe(
      tap((res: any) => {
        if(res){
          this.toastr.success('Sửa thông tin giảng viên thành công', 'Success');
        } else {
          this.toastr.danger(res.message, 'Error');
        }
        
      }, err => {
        this.toastr.danger('Có lỗi xảy ra', 'Error');
      }),
    );

  }
  
  createLecturer(lecturerInfo: LecturerSubmit): Observable<ResponseDataModel> {
    console.log("tesst", lecturerInfo)
    return this.http.post(`${environment.apiUrl}/admin/lecturer`, lecturerInfo).pipe(
      tap((res: any) => {
        if(res.status) {
          this.toastr.success('Tạo mới giảng viên chủ nhiệm thành công', 'Success');

        } else {
          this.toastr.danger(res.message, 'Error');
        }
      }, err => {
        this.toastr.danger('Có lỗi xảy ra', 'Error');
      }),
    );

  }

  updateLecturer(lecturerInfo: LecturerSubmit, id:number): Observable<ResponseDataModel> {
    console.log("tesst", lecturerInfo)
    return this.http.put(`${environment.apiUrl}/admin/lecturer/${id}`, lecturerInfo).pipe(
      tap((res: any) => {
        if(res.status){
          this.toastr.success('Sửa thông tin giảng viên thành công', 'Success');
        } else {
          this.toastr.danger(res.message, 'Error');
        }
        
      }, err => {
        this.toastr.danger('Có lỗi xảy ra', 'Error');
      }),
    );

  }

  createNotification(notification: NotificationSubmit): Observable<ResponseDataModel> {
    console.log("questionInfo", notification)
    return this.http.post(`${environment.apiUrl}/admin/notification`, notification).pipe(
      tap((res: any) => {
        this.toastr.success('Tạo mới thông báo thành công', 'Success');
      }, err => {
        this.toastr.danger('Có lỗi xảy ra', 'Error');
      }),
    );

  }

  createQuestion(questionInfo: QuestionSubmit): Observable<ResponseDataModel> {
    console.log("questionInfo", questionInfo)
    return this.http.post(`${environment.apiUrl}/admin/questions`, questionInfo).pipe(
      tap((res: any) => {
        this.toastr.success('Tạo mới câu hỏi thành công', 'Success');
      }, err => {
        this.toastr.danger('Có lỗi xảy ra', 'Error');
      }),
    );

  }

  async listPaperMaster(): Promise<Paper[]> {
    let dataMaster: Paper[] = [];

    const data = await this.http.get<any>(`${environment.apiUrl}/admin/paper`)
      .toPromise();

    if (await data) {
      // this.toastr.success('Lấy dữ liệu thống kê thành công', 'Success');
      dataMaster = data.data;
    }
    else {
      // this.toastr.danger('Có lỗi xảy ra', 'Error');
    }
    return dataMaster;
  }

  async listPaperStudent(code:string): Promise<Paper[]> {
    const params = new HttpParams()
      .set('code', code);
    // return 
    let papers = [];

    const data = await this.http.get<any>(`${environment.apiUrl}/admin/paper/students`, {params: params})
      .toPromise();

    if (await data) {
      // this.toastr.success('Lấy dữ liệu thống kê thành công', 'Success');
      papers = data.data;
    }
    else {
      // this.toastr.danger('Có lỗi xảy ra', 'Error');
    }
    return papers;
  }

  submitPaper(papers, code): Observable<ResponseDataModel> {

    const req = new Body();
    req.code = code;
    req.papers = papers;


    return this.http.post(`${environment.apiUrl}/admin/students/submit-paper`, req).pipe(
      tap((res: any) => {
        if(res.status) {
          this.toastr.success('Lưu thành công!', 'Success');
          window.location.reload();
        } else {
          this.toastr.danger(res.message, 'Error');
        }
      }, err => {
        this.toastr.danger('Có lỗi xảy ra', 'Error');
      }),
    );

  }

  one(id: number): Observable<Post> {
    return this.http
      .get<RestResponse<Post>>(`${environment.apiUrl}/user/${id}`)
      .pipe(
        map(rs => rs.data),
      );
  }

  activeUser(id: number): Observable<Post> {
    return this.http
      .put<RestResponse<Post>>(`${environment.apiUrl}/user/active/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }

  inactiveUser(id: number): Observable<Post> {
    return this.http
      .put<RestResponse<Post>>(`${environment.apiUrl}/user/inactive/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }
}
