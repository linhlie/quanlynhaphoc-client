import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PageResponse} from '../dto/page-response';
import {Topic} from '../models/topic.model';
import {environment} from '../../environments/environment';
import {RestResponse} from '../dto/rest-response';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TopicService {

  constructor(private http: HttpClient) { }

  list(page: number = 1, limit: number = 10): Observable<PageResponse<Topic[]>> {
    const params = new HttpParams()
      .set('limit', limit.toString())
      .set('page', page.toString());
    return this.http.get<PageResponse<Topic[]>>(`${environment.apiUrl}/topic`, {params: params});
  }

  create(topic: Topic): Observable<Topic> {
    return this.http.post<RestResponse<Topic>>(`${environment.apiUrl}/topic`, topic)
      .pipe(
        map(res => res.data),
      );
  }

  update(topic: Topic): Observable<Topic> {
    return this.http.put<RestResponse<Topic>>(`${environment.apiUrl}/topic/${topic.id}`, topic)
      .pipe(
        map(res => res.data),
      );
  }
}
