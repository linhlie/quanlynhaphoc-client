import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Post} from '../models/post.model';
import {PageResponse} from '../dto/page-response';
import {environment} from '../../environments/environment';
import {RestResponse} from '../dto/rest-response';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PostService {

  constructor(private http: HttpClient) { }

  list(page: number = 1, limit: number = 5): Observable<PageResponse<Post[]>> {
    const params = new HttpParams()
      .set('limit', limit.toString())
      .set('page', page.toString());
    return this.http.get<PageResponse<Post[]>>(`${environment.apiUrl}/admin/posts`, {params: params});
  }

  one(id: number): Observable<Post> {
    return this.http
      .get<RestResponse<Post>>(`${environment.apiUrl}/admin/posts/${id}`)
      .pipe(
        map(rs => rs.data),
      );
  }

  activePost(id: number): Observable<Post> {
    return this.http
      .put<RestResponse<Post>>(`${environment.apiUrl}/admin/posts/active/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }

  inactivePost(id: number): Observable<Post> {
    return this.http
      .put<RestResponse<Post>>(`${environment.apiUrl}/admin/posts/inactive/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }
}
