import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User, UserChangePassword, UserSubmit} from '../models/user.model';
import {PageResponse} from '../dto/page-response';
import {environment} from '../../environments/environment';
import {RestResponse} from '../dto/rest-response';
import {map} from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';
import { ResponseDataModel } from '../models/response-data.model';
import HttpStatusCode from '../models/HttpStatusCode.model';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root',
})
export class UserService {

  constructor(private http: HttpClient, private toastr: NbToastrService,private router: Router) { }

  list(page: number = 1, limit: number = 5): Observable<PageResponse<User[]>> {
    const params = new HttpParams()
      .set('limit', limit.toString())
      .set('page', page.toString());
    return this.http.get<PageResponse<User[]>>(`${environment.apiUrl}/admin/users`, {params: params});
  }

  one(id: number): Observable<User> {
    return this.http
      .get<RestResponse<User>>(`${environment.apiUrl}/admin/users/${id}`)
      .pipe(
        map(rs => rs.data),
      );
  }

  activeUser(id: number): Observable<User> {
    return this.http
      .put<RestResponse<User>>(`${environment.apiUrl}/admin/users/active/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }

  inactiveUser(id: number): Observable<User> {
    return this.http
      .put<RestResponse<User>>(`${environment.apiUrl}/admin/users/inactive/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }

  CreateUser(userInfo: UserSubmit): Observable<ResponseDataModel> {
    console.log("tesst", userInfo)
    return this.http.post(`${environment.apiUrl}/admin/users`, userInfo).pipe(
      tap((res: any) => {
        if(res.status) {
          this.toastr.success('Tạo mới người dùng thành công', 'Success');
          this.router.navigate(['pages/users']);
        } else {
          this.toastr.danger(res.message, 'Error');
        }
      }, err => {
        this.toastr.danger('Có lỗi xảy ra', 'Error');
      }),
    );

  }
  
  UpdateUser(user: UserSubmit, userId: string): Observable<ResponseDataModel> {

    return this.http.put(`${environment.apiUrl}/admin/users/` + userId, user).pipe(
      tap((res: any) => {
        console.log(res.status)
        if (res.status) {
          this.toastr.success('Cập nhật người dùng thành công', 'Success');
          this.router.navigate(['pages/users']);
        }
        // tslint:disable-next-line: triple-equals
        else if (res.status == HttpStatusCode.PAYMENT_REQUIRED) {
          this.toastr.danger('Không có quyền cập nhật tài khoản này', 'Error');
        }
        // tslint:disable-next-line: triple-equals
        else if (res.status == HttpStatusCode.FORBIDDEN) {
          this.toastr.danger('Không có quyền cập nhật tài khoản này', 'Error');
        }
        else {
          this.toastr.danger(res.status, 'Error');
        }
      }, err => {
        this.toastr.danger('Có lỗi xảy ra', 'Error');
      }),
    );

  }
}
