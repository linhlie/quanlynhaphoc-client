import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../models/student.model';
import { PageResponse } from '../dto/page-response';
import { environment } from '../../environments/environment';
import { RestResponse } from '../dto/rest-response';
import { map } from 'rxjs/operators';
import { tap } from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';


@Injectable({
  providedIn: 'root',
})
export class ClassService {

  constructor(private http: HttpClient, private toastr: NbToastrService) { }

  list(page: number = 1, limit: number = 5) {
    const params = new HttpParams()
      .set('limit', limit.toString())
      .set('page', page.toString());
    return this.http.get<any>(`${environment.apiUrl}/admin/students`, { params: params });
  }

  saveClass(param) {
    const params = new HttpParams()
    return this.http
      .post<RestResponse<any>>(`${environment.apiUrl}/admin/students/save-class`, param)
      .pipe(
        tap((res: any) => {
          if(res.status) {
            this.toastr.success('Tạo lớp thành công', 'Success');
            window.location.reload();
          } else {
            this.toastr.danger('Có lỗi xảy ra', 'Error');
          }
        }, err => {
          this.toastr.danger('Có lỗi xảy ra', 'Error');
        }),
      );
  }

  updateClass(param, id) {
    const params = new HttpParams()
    return this.http
      .put<RestResponse<any>>(`${environment.apiUrl}/admin/class/`+ id, param)
      .pipe(
        tap((res: any) => {
          if(res.status) {
            this.toastr.success('Chỉnh sửa thành công', 'Success');
          } else {
            this.toastr.danger('Có lỗi xảy ra', 'Error');
          }
        }, err => {
          this.toastr.danger('Có lỗi xảy ra', 'Error');
        }),
      );
  }

  classList(industry_id:string){
    const params = new HttpParams()
    .set('industry_id', industry_id)
    let data:any;
    data = this.http.get<any>(`${environment.apiUrl}/admin/class`, { params: params });
    console.log("data", data)
    return data;
  }

  studentClass(item){
    const params = new HttpParams()
    .set('class_id', item.id)
    .set('industry_id',item.industry_id)
    return this.http.get<any>(`${environment.apiUrl}/admin/students/class`, { params: params });
     
  }

  one(id: number): Observable<any> {
    console.log("call")
    return this.http.get<any>(`${environment.apiUrl}/admin/class/${id}`)
  }

  getClassInsert(form): Observable<any> {
    const params = new HttpParams()
    .set('orderBy', 'name')
    .set('industry_id', form.industry_id)
    .set('nameFormat', form.nameFormat)
    .set('total_class', form.total_class);
    return this.http.get<any>(`${environment.apiUrl}/admin/students/insert-class`, { params: params });
  }
  

  activeUser(id: number): Observable<Student> {
    return this.http
      .put<RestResponse<Student>>(`${environment.apiUrl}/user/active/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }

  inactiveUser(id: number): Observable<Student> {
    return this.http
      .put<RestResponse<Student>>(`${environment.apiUrl}/user/inactive/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }

  insertStudent(fileToUpload: File): Observable<Student> {
    const formData: FormData = new FormData();
    formData.append('select_file', fileToUpload, fileToUpload.name);
    return this.http
      .post<RestResponse<Student>>(`${environment.apiUrl}/excel`, formData)
      .pipe(
        map(res => res.data),
      );
  }
}
