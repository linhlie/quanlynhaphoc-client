import {RestResponse} from './rest-response';

export interface PageResponse<T> extends RestResponse<T> {
  pageNumber: number;
  pageSize: number;
  totalPages: number;
  totalRecords: number;
}
