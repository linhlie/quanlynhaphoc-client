export interface RestResponse<T> {
  status: boolean;
  message: string;
  data: T;
}
