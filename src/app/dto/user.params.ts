export interface UserParams {
  rankOrder?: string;
  priceOrder?: string;
  rank?: number;
  minPrice?: number;
  maxPrice?: number;
  related?: string;
  topicIds?: string;
}
