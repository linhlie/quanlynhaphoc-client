import { Component, OnInit } from '@angular/core';
import {ClassService} from '../../services/class.service';
import {PageResponse} from '../../dto/page-response';
import {Student} from '../../models/student.model';
import {PageEvent} from '@angular/material/paginator';
import { FormBuilder, FormGroup } from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {DialogChangeClassComponent} from './dialog-change-class/dialog-change-class.component';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'ngx-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.scss'],
})
export class ClassComponent implements OnInit {
  student:any = [];
  studentClass: any;
  listClass: any = [];
  classIndex:any=[];
  displayedColumns = ['id', 'full_name', 'code', 'id_card', 'exam_id', 'address', 'phone', 'status', 'gender', 'date_of_birth', 'actions'];
  checkoutForm : FormGroup;
  class:any = 0;
  classList:any;
  dataRequest:any;
  displayedColumnsClass = ['id', 'name', 'industry_id', 'lecturer_name', 'total_student', 'actions']
  constructor(private classService: ClassService, private formBuilder: FormBuilder, public dialog: MatDialog,  private router:Router) { }

  ngOnInit(): void {
    this.checkoutForm = this.formBuilder.group({
      nameFormat: '',
      total_class:'',
      orderBy: 'name',
      industry_id: '',
      class:[0]
    });

    // this.classService.list()
    //   .subscribe(rs => this.student = rs.data);

    this.classList = this.classService.classList('')
      .subscribe(rs => this.classList = rs.data);
    
      console.log('classList', this.classList)
  }

  openClass(item):void {
    this.classService.studentClass(item)
      .subscribe(rs => this.student = rs.data);
  }

  onSubmit(): void {

    this.classService.getClassInsert(this.checkoutForm.value).subscribe(
      (rs:any) => { 
        this.listClass = rs.data;
        this.classIndex = rs.data ? Object.keys(rs.data) : [];
        this.class = 0;
        console.log(this.checkoutForm.value.class)
        this.filterStudent();
        
      });
    
    this.dataRequest = this.checkoutForm.value;

    this.checkoutForm.reset();
  }

  filterStudent(){
    this.student = [...this.listClass[this.class]];
    
  }
  
  pageChange($event: PageEvent) {
    this.classService.list($event.pageIndex + 1, $event.pageSize)
      .subscribe(rs => this.student = rs);
  }

  save(){
    
    let param  = {
      industry_id: this.dataRequest.industry_id,
      nameFormat: this.dataRequest.nameFormat,
      students: this.listClass
    }

    this.student = this.classService.saveClass(param);
  }

  openDialogChangeClass(element:any){
    let item:any = {
      index:this.classIndex,
      studentSelected: element,
      listClass:this.listClass,
      classSelected: this.class
    }
    const dialogRef = this.dialog.open(DialogChangeClassComponent,{
      data:{item},
      disableClose:true,
      width:'200px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result:`,item.listClass);
      item.listClass.forEach(element => {
        const result = element.sort((a, b) => a.name.localeCompare(b.name));     
      });
      
      this.listClass = item.listClass;
      this.filterStudent();
    });
  }

  roleHighlight(userRole: string) {
    switch (userRole) {
      case 'ADMIN': return 'highlight-admin';
      case 'VISITOR': return 'highlight-visitor';
      case 'COLLABORATOR': return 'highlight-collab';
    }
  }

  statusHighlight(status: number) {
    return status === 6 ? 'highlight-active' : 'highlight-inactive';
  }
}
