import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogChangeClassComponent } from './dialog-change-class.component';

describe('DialogChangeClassComponent', () => {
  let component: DialogChangeClassComponent;
  let fixture: ComponentFixture<DialogChangeClassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogChangeClassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogChangeClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
