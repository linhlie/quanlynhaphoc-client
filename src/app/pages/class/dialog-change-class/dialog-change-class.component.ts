import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'ngx-dialog-change-class',
  templateUrl: './dialog-change-class.component.html',
  styleUrls: ['./dialog-change-class.component.scss']
})
export class DialogChangeClassComponent implements OnInit {
  checkoutForm : FormGroup;
  constructor(@Inject(MAT_DIALOG_DATA) public data:any, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.checkoutForm = this.formBuilder.group({
      class :0
    });
  }
  changeClass(){
    
  }
  save(){
    this.data.item.listClass[this.checkoutForm.value.class].push(this.data.item.studentSelected);
    console.log('b',this.data.item.listClass[this.data.item.classSelected].indexOf(this.data.item.studentSelected))
    let index = this.data.item.listClass[this.data.item.classSelected].indexOf(this.data.item.studentSelected)
    let item = this.data.item.listClass[this.data.item.classSelected].splice(index,1);
    console.log()
  }
}
