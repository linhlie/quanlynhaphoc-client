import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../services/home.service';
import { Question } from '../../models/question.model';
import {PageResponse} from '../../dto/page-response';
import {PageEvent} from '@angular/material/paginator';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'ngx-question-manager',
  templateUrl: './question-manager.component.html',
  styleUrls: ['./question-manager.component.scss']
})
export class QuestionManagerComponent implements OnInit {

  constructor( private homeService:HomeService, private router:Router) { }

  questions: PageResponse<Question[]>;
  displayedColumns = ['id', 'question', 'answer', 'status', 'actions'];

  ngOnInit(): void {
    this.homeService.listQuestion()
    .subscribe(rs => this.questions = rs);
  }

  pageChange($event: PageEvent) {
    this.homeService.listQuestion($event.pageIndex + 1, $event.pageSize)
      .subscribe(rs => this.questions = rs);
  }

  statusHighlight(status: number) {
    return status === 1 ? 'highlight-active' : 'highlight-inactive';
  }
}
