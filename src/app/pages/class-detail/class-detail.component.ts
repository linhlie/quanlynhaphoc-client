import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ClassModel, ClassModelSubmit } from '../../models/class.model';
import { ClassService } from '../../services/class.service';
import { PageResponse } from '../../dto/page-response';
import { Lecturer } from '../../models/lecturer.model';
import { HomeService } from '../../services/home.service';

@Component({
  selector: 'ngx-class-detail',
  templateUrl: './class-detail.component.html',
  styleUrls: ['./class-detail.component.scss']
})
export class ClassDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private homeService:HomeService, private route2: Router, private formBuilder: FormBuilder, private classService: ClassService) { }

  isEdit = false;

  classForm: FormGroup;
  id: number;
  selectedClass: ClassModel;

  submitClass!: ClassModelSubmit;

  lecturer: PageResponse<Lecturer[]>;

  // model = new ClassModel(18, 'Dr IQ', this.powers[0], 'Chuck Overstreet');

  ngOnInit() {

    this.classForm = this.formBuilder.group({
      name: '',
      code: '',
      total_student: '',
      lecturer_name: '',
      industry_name: '',
      industry_id: '',
    });

    this.selectedClass = {
      industry_id: '',
      code: '',
      created_at: '',
      deleted_at: '',
      id: 0,
      id_lecturer: 0,
      industry_name: '',
      lecturer_name: '',
      name: '',
      total_student: 0,
      updated_at: '',
      
    }


    this.homeService.listLecturer()
    .subscribe(rs => this.lecturer = rs);


    this.route.params.subscribe(params => {
      if(params['id']) {
        this.id = params['id'];
        this.isEdit = true;

        this.classService.one(params['id'])
          .subscribe(rs => this.selectedClass = rs.data);
      }
      
    });
  }
  onSubmit(): any {
    const submitClass: ClassModelSubmit = this.classForm.value;
    console.log("submitClass", submitClass)
    if(this.isEdit) {
      this.classService.updateClass(submitClass, this.id).subscribe((res) => {
        console.log(res);
        this.route2.navigate(['pages/class']);
      });
    } else {
      // this.userService.CreateUser(submitUser).subscribe((res) => {
      //   console.log(res);
      //   this.router.navigate(['pages/users']);
      // });
    }

  }

  onCancel(): void {
    this.route2.navigate(['pages/users']);
  }
}
