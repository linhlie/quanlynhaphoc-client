import { Component, OnInit } from '@angular/core';
import { NbMenuItem, NbMenuService } from '@nebular/theme';
// import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { AuthService } from '../auth/auth.service';
import { MENU_ITEMS,MENU_ITEMS_NV, Role } from './pages-menu';
import { User } from '../models/user.model';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import firebase from "firebase/app";
import "firebase/firestore";
// const config = {
//   apiKey: 'AIzaSyB0PR_P6nUWUlxBNrLJqNvRbkeqzEq9gyM',
//   databaseURL: 'https://quanlynhaphoc-default-rtdb.firebaseio.com'
// };
@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})


export class PagesComponent implements OnInit {

  sidebarMenu: NbMenuItem[] = [];
  user: any = this.auth.getPayload();
  constructor( private auth: AuthService) {
      // firebase.initializeApp(config);
  }

  ngOnInit(): void {
    console.log("user", this.user)
    if (this.user.role_id === Role.ADMIN) {
      this.sidebarMenu = MENU_ITEMS;
    } else {
      this.sidebarMenu = MENU_ITEMS_NV;
    }

    // this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
    //   this.translateMenuItems();
    // });
    // this.translateMenuItems();
  }


  translateMenuItems(): any {
    this.sidebarMenu.forEach(item => {
      this.translateMenuItem(item);
    });
  }

  translateMenuItem(menuItem: NbMenuItem): any {
    if (menuItem.children != null) {
      menuItem.children.forEach(item => this.translateMenuItem(item));
    }

    if (menuItem.data === undefined) {
      menuItem.data = menuItem.title;
      // menuItem.title = this.translate.instant(menuItem.title);
    } else {
      // menuItem.title = this.translate.instant(menuItem.data);
    }
  }

}
