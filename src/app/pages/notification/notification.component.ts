import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../services/home.service';
import { Question } from '../../models/question.model';
import {PageResponse} from '../../dto/page-response';
import {PageEvent} from '@angular/material/paginator';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Notification } from '../../models/notification.model';

@Component({
  selector: 'ngx-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  constructor( private homeService:HomeService, private router:Router) { }

  notifications: PageResponse<Notification[]>;
  displayedColumns = ['id', 'content', 'type', 'id_student', 'start_date', 'end_date', 'status', 'actions'];

  ngOnInit(): void {
    this.homeService.listNotifications()
    .subscribe(rs => this.notifications = rs);
  }

  pageChange($event: PageEvent) {
    this.homeService.listNotifications($event.pageIndex + 1, $event.pageSize)
      .subscribe(rs => this.notifications = rs);
  }

  statusHighlight(status: number) {
    return status === 1 ? 'highlight-active' : 'highlight-inactive';
  }

}
