import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User, UserSubmit } from '../../models/user.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ngx-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {

  isEdit = false;
  id = '';
  userForm : FormGroup;
  selectedUser !: User;

  submitUser!: UserSubmit;

  constructor(private router: Router, private routeBeta: ActivatedRoute, private userService: UserService, private http: HttpClient,  private formBuilder: FormBuilder,) { }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      birthDate: '',
      email: '',
      name: '',
      password: '',
      role_id: '',
      status: 1,
      username: '',
      phone: '',
      avatar:'',
      address: '',
      gender: '',
    });

    this.selectedUser = {
      id: 1,
      birthDate: '',
      email: '',
      name: '',
      password: '',
      role_id: 1,
      status: 0,
      username: '',
      phone: '',
      avatar:'',
      address: '',
      gender: '',
    }

    this.routeBeta.params.subscribe(params => {
      if(params['id']) {
        this.isEdit = true;
        this.id = params['id'];
        this.userService.one(params['id'])
          .subscribe(rs => this.selectedUser = rs);
      }
      
    });

  }

  onSubmit(): any {
    const submitUser: UserSubmit = this.userForm.value;

    if(this.isEdit) {
      this.userService.UpdateUser(submitUser, this.id).subscribe((res) => {
        console.log(res);
      });
    } else {
      this.userService.CreateUser(submitUser).subscribe((res) => {
        console.log(res);
      });
    }

  }

  onCancel(): void {
    this.router.navigate(['pages/users']);
  }

}
