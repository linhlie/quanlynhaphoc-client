import {Component, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StudentService} from '../../services/student.service';
import {Student} from '../../models/student.model';
import {StatisticService} from '../../services/statistic.service';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import { HomeService } from '../../services/home.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { from } from 'rxjs';
@Component({
  selector: 'ngx-students-details',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.scss'],
})
export class StudentDetailComponent implements OnInit {
  student: Student;
  status: any
  form : FormGroup;
  id: 0;
  changUserStatusDialogRef: NbDialogRef<any>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private studentService: StudentService,
              private statisticService: StatisticService,
              public dialogService: NbDialogService,
              private toastrService: NbToastrService,private homeService:HomeService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.studentService.one(params['id']).subscribe(rs => {
        this.student = rs;
        // this.graph.data.datasets[0].label = rs;
        this.id = params['id'];
        this.form = this.formBuilder.group({
          status: '',
          idStudent: params['id']
        })
      });
      this.homeService.status()
      .subscribe(rs => this.status = rs.data);
    });

    // this.form = this.formBuilder.group({
    //   status: '',
    //   id_student: this.id
    // });
  }

  openChangeUserStatusDialog(ref: TemplateRef<any>): void {
    this.changUserStatusDialogRef = this.dialogService
      .open(ref, {context: this.student.status === 1 ? 'inactive' : 'active'});
  }

  changeUserStatus() {
    const form: any = this.form.value;
    console.log("object", form);

    this.homeService.updateStatusStudent(form).subscribe((res) => {
      console.log(res);
      this.router.navigate(['pages/students']);
    });
    this.homeService.updateStatusStudent(form)
      .subscribe(rs => this.status = rs.data);
    this.changUserStatusDialogRef.close();
  }
}
