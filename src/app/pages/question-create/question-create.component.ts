import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User, UserSubmit } from '../../models/user.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { HttpClient } from '@angular/common/http';
import { HomeService } from '../../services/home.service';
import { Question } from '../../models/question.model';
import { QuestionSubmit } from '../../models/question.model';
@Component({
  selector: 'ngx-question-create',
  templateUrl: './question-create.component.html',
  styleUrls: ['./question-create.component.scss']
})
export class QuestionCreateComponent implements OnInit {

  isEdit = false;
  id = 0;
  questionForm : FormGroup;

  submitQuestion!: QuestionSubmit;

  question!: Question;

  constructor(private router: Router, private route: ActivatedRoute, private homeService: HomeService, private http: HttpClient,  private formBuilder: FormBuilder,) { }

  ngOnInit(): void {
    this.questionForm = this.formBuilder.group({
      question: '',
      type: 1 ,
      answer:'',
      status: '',
    });

    this.route.params.subscribe(params => {
      if(params['id']) {
        this.isEdit = true;
        this.id = params['id'];
        this.homeService.question(params['id']).subscribe(rs => {
          this.question = rs.data;
        });
      }
      
    });
    
  }

  onSubmit(): any {
    const questionSubmit: QuestionSubmit = this.questionForm.value;
    
      this.homeService.createQuestion(questionSubmit).subscribe((res) => {
        console.log(res);
        this.router.navigate(['pages/question-manager']);
      });

  }

  onCancel(): void {
    this.router.navigate(['pages/question-manager']);
  }

}
