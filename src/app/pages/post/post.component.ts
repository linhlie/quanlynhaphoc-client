import { Component, OnInit, TemplateRef } from '@angular/core';
import { PostService } from '../../services/post.service';
import { PageResponse } from '../../dto/page-response';
import { Post } from '../../models/post.model';
import { PageEvent } from '@angular/material/paginator';
import {ActivatedRoute} from '@angular/router';
import { NbDialogRef, NbDialogService, NbToastrService } from '@nebular/theme';
@Component({
  selector: 'ngx-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {
  post: PageResponse<Post[]>;
  displayedColumns = ['id', 'fullName', 'status', 'actions'];
  changPostStatusDialogRef: NbDialogRef<any>;

  id: any;
  postTemp: Post;
  constructor(private postService: PostService,
    private route: ActivatedRoute,
    public dialogService: NbDialogService,
    private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.postService.list()
      .subscribe(rs => this.post = rs);
  }

  pageChange($event: PageEvent) {
    this.postService.list($event.pageIndex + 1, $event.pageSize)
      .subscribe(rs => this.post = rs);
  }

  openChangePostStatusDialog(ref: TemplateRef<any>, id): void {
    this.postTemp = this.post.data.find(o => o.id === id);
    this.changPostStatusDialogRef = this.dialogService
      .open(ref, {context: this.postTemp.status === 1 ? 'inactive' : 'active'});
  }

  changePostStatus() {

    if (this.postTemp.status === 1) {
      this.postService.inactivePost(this.postTemp.id)
        .subscribe(res => {
          this.postTemp = res;
          this.toastrService.success('', 'Successfully');
          window.location.reload();
        }, err => this.toastrService.danger(err, 'Failed'));
    } else {
      this.postService.activePost(this.postTemp.id)
        .subscribe(res => {
          this.postTemp = res;
          this.toastrService.success('', 'Successfully');
          window.location.reload();
        }, err => this.toastrService.danger(err, 'Failed'));
    }
    this.changPostStatusDialogRef.close();
  }

  roleHighlight(postRole: string) {
    switch (postRole) {
      case 'ADMIN': return 'highlight-admin';
      case 'VISITOR': return 'highlight-visitor';
      case 'COLLABORATOR': return 'highlight-collab';
    }
  }

  statusHighlight(status: number) {
    return status === 1 ? 'highlight-active' : 'highlight-inactive';
  }
}
