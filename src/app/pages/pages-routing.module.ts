import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './not-found/not-found.component';
import {UserComponent} from './user/user.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {TopicComponent} from './topic/topic.component';
import {StudentsComponent} from './students/students.component';
import {ClassComponent} from './class/class.component';
import {PostComponent} from './post/post.component';
import {StudentDetailComponent} from './student-detail/student-detail.component';
import {MasterManagerComponent} from './master-manager/master-manager.component';
import {PaperSubmissionComponent} from './paper-submission/paper-submission.component';
import {UserCreateComponent} from './user-create/user-create.component';
import { AuthGuard } from '../auth/auth.guard';
import {ClassDetailComponent} from './class-detail/class-detail.component';
import {MasterCreateComponent} from './master-create/master-create.component';
import {LecturerComponent} from './lecturer/lecturer.component';
import {QuestionManagerComponent} from './question-manager/question-manager.component';
import {LecturerCreateComponent} from './lecturer-create/lecturer-create.component';
import { QuestionCreateComponent } from './question-create/question-create.component';
import { NotificationComponent } from './notification/notification.component';
import { NotificationCreateComponent } from './notification-create/notification-create.component';
import { ChatManagerComponent } from './chat-manager/chat-manager.component';
import { from } from 'rxjs';
import { ChatRoomComponent } from './chat-room/chat-room.component';
import { RoomlistComponent } from './roomlist/roomlist.component';
import { AddroomComponent } from './addroom/addroom.component';
const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'users',
      component: UserComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'users/new',
      component: UserCreateComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'users/new/:id',
      component: UserCreateComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'users/:id',
      component: UserDetailsComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'topics',
      component: TopicComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'students',
      component: StudentsComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'students/:id',
      component: StudentDetailComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'class',
      component: ClassComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'class/:id',
      component: ClassDetailComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'posts',
      component: PostComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'master-manager',
      component: MasterManagerComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'paper',
      component: PaperSubmissionComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'master-create',
      component: MasterCreateComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'lecturer',
      component: LecturerComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'lecturer-create',
      component: LecturerCreateComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'lecturer-create/:id',
      component: LecturerCreateComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'question-manager',
      component: QuestionManagerComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'question-create',
      component: QuestionCreateComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'question-create/:id',
      component: QuestionCreateComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'notification-manager',
      component: NotificationComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'notification-create',
      component: NotificationCreateComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'notification/:id',
      component: NotificationCreateComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'chat-manager',
      component: ChatManagerComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'chat-room',
      component: ChatRoomComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'room',
      component: RoomlistComponent,
      canActivate: [AuthGuard],
    },
    {
      path: 'addroom',
      component: AddroomComponent,
      canActivate: [AuthGuard],
    },
    { 
      path: 'chat-room/:roomname', 
      component: ChatRoomComponent 
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
