import { Component, OnInit } from '@angular/core';
// import { routes } from '../../app-routing.module';
import { StatisticService } from '../../services/statistic.service';
import { StudentService } from '../../services/student.service';
// import {routes} from '../../app-routing.module'
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  overview: any;
  userStructureGraph: any;
  financeGraph: any;

  fileToUpload: File = null;

  constructor(private statisticService: StatisticService, private studentService: StudentService,) { }
  // constructor(private studentService: StudentService) { }

  ngOnInit(): void {
    this.statisticService.statOverview()
      .subscribe(data => this.overview = data);
  }

  // pageChange($event: PageEvent) {
  //   this.userService.list($event.pageIndex + 1, $event.pageSize)
  //     .subscribe(rs => this.user = rs);
  // }

  handleFileInput(files: FileList) {
    console.log("handel")
    // this.fileToUpload = files.item(0);
    this.studentService.insertStudent(files.item(0)).subscribe(data => {
      window.location.reload();
      }, error => {
        console.log(error);
      });
  }
}


