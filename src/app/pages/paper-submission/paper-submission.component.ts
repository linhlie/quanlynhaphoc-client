import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { HomeService } from '../../services/home.service';
import { Paper } from '../../models/paper.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'ngx-paper-submission',
  templateUrl: './paper-submission.component.html',
  styleUrls: ['./paper-submission.component.scss']
})
export class PaperSubmissionComponent implements OnInit {

  papers: Paper[] = [];
  code: string;
  paperList = [];

  paperMaster: Paper[] = [];

  form: FormGroup;

  constructor(private homeService: HomeService, private route: ActivatedRoute, private formBuilder: FormBuilder) {

  }

  async ngOnInit(): Promise<void> {

    const code = this.route.snapshot.paramMap.get('code');

    let data = [];
    let data2 = [];
    data = await this.homeService.listPaperMaster();

    data2 = await this.homeService.listPaperStudent(code);
    this.papers = data2;

    if (data2.length > 0) {
      for (var i = 0; i < data.length; i++) {
        var found = false;

        for (var j = 0; j < data2.length; j++) { // j < is missed;
          if (data[i].id == data2[j].type) {
            found = true;
            break;
          }
        }
        if (found == false) {
          this.paperMaster.push(data[i]);
        }
      }
    } else {
      this.paperMaster = data
    }
  }

  submitForm() {
    console.log("list", this.paperList)
    this.homeService.submitPaper(this.paperList, this.route.snapshot.paramMap.get('code')).subscribe((res) => {
    });
  }

  onCheckboxChange(e) {
    
    const index = this.paperList.indexOf(e.target.value);
    if (index > -1) {
      this.paperList.splice(index, 1);
    } else {
      this.paperList.push(e.target.value)
    }
    console.log("list", this.paperList)
  }

}
