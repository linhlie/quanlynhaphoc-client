import {Component, OnInit, TemplateRef} from '@angular/core';
import {PageResponse} from '../../dto/page-response';
import {Topic} from '../../models/topic.model';
import {PageEvent} from '@angular/material/paginator';
import {TopicService} from '../../services/topic.service';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';

@Component({
  selector: 'ngx-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss'],
})
export class TopicComponent implements OnInit {
  topics: PageResponse<Topic[]>;
  displayedColumns = ['id', 'title', 'actions'];
  createTopicDialogRef: NbDialogRef<any>;
  updateTopicDialogRef: NbDialogRef<any>;
  topic2Create = new Topic;
  topic2Update: Topic;

  constructor(private topicService: TopicService,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.topicService.list()
      .subscribe(rs => this.topics = rs);
  }

  pageChange($event: PageEvent) {
    this.topicService.list($event.pageIndex + 1, $event.pageSize)
      .subscribe(rs => this.topics = rs);
  }

  createTopic() {
    this.topicService.create(this.topic2Create)
      .subscribe(res => {
        this.topics.data = [res, ...this.topics.data];
        this.toastrService.success('', 'Successfully');
      }, err => {
        this.toastrService.danger(err, 'Failed');
      });
    this.createTopicDialogRef.close();
  }

  openCreateTopicDialog(dialogRef: TemplateRef<any>) {
    this.createTopicDialogRef = this.dialogService.open(dialogRef);
  }

  openUpdateTopicDialog(dialogRef: TemplateRef<any>, topic: Topic) {
    this.topic2Update = {...topic};
    this.updateTopicDialogRef = this.dialogService.open(dialogRef);
  }

  updateTopic() {
    this.topicService.update(this.topic2Update)
      .subscribe(res => {
        this.topics.data = this.topics.data.map(t => t.id === res.id ? res : t);
        this.toastrService.success('', 'Successfully');
      }, err => {
        this.toastrService.danger(err, 'Failed');
      });
    this.updateTopicDialogRef.close();
  }
}
