import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { QuestionSubmit } from '../../models/question.model';
import { NotificationSubmit, Notification } from '../../models/notification.model';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HomeService } from '../../services/home.service';

@Component({
  selector: 'ngx-notification-create',
  templateUrl: './notification-create.component.html',
  styleUrls: ['./notification-create.component.scss']
})
export class NotificationCreateComponent implements OnInit {

  isEdit = false;
  id = 0;
  notificationForm : FormGroup;

  submitNotification!: NotificationSubmit;

  notification!: Notification;

  constructor(private router: Router, private route: ActivatedRoute, private homeService: HomeService, private http: HttpClient,  private formBuilder: FormBuilder,) { }

  ngOnInit(): void {
    this.notificationForm = this.formBuilder.group({
      content:'',
      type: 1,
      id_student:'',
      status: 1,
      start_date: '',
      end_date: '',
    });

    this.route.params.subscribe(params => {
      if(params['id']) {
        this.isEdit = true;
        this.id = params['id'];
        this.homeService.notification(params['id']).subscribe(rs => {
          this.notification = rs.data;
        });
      }
      
    });
    
  }

  onSubmit(): any {
    const notification: NotificationSubmit = this.notificationForm.value;
    
      this.homeService.createNotification(notification).subscribe((res) => {
        console.log(res);
        this.router.navigate(['pages/notification-manager']);
      });

  }

  onCancel(): void {
    this.router.navigate(['pages/notification-manager']);
  }


}
