import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User, UserSubmit } from '../../models/user.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { HttpClient } from '@angular/common/http';
import { HomeService } from '../../services/home.service';
import { Lecturer,  LecturerSubmit} from '../../models/lecturer.model';
import * as moment from 'moment';

@Component({
  selector: 'ngx-lecturer-create',
  templateUrl: './lecturer-create.component.html',
  styleUrls: ['./lecturer-create.component.scss']
})
export class LecturerCreateComponent implements OnInit {

  isEdit = false;
  id = 0;
  lecturerForm : FormGroup;

  submitLecturer!: LecturerSubmit;

  lecturer!: Lecturer;

  constructor(private route: ActivatedRoute, private router: Router, private homeService: HomeService, private http: HttpClient,  private formBuilder: FormBuilder,) { }

  ngOnInit(): void {
    this.lecturerForm = this.formBuilder.group({
      date_of_birth: '',
      email: '',
      name: '',
      status: 1,
      phone: '',
      address: '',
      gender: '',
      id_faculty:'',
      code: '',
      rank: '',
      deleted_at: '',
    });

    this.lecturer = {
      id: 1,
      name: '',
      email: '',
      address: '',
      gender: '',
      phone: '',
      status: 1,
      code:'',
      id_faculty: '',
      date_of_birth: '',
      rank: '',
      deleted_at: ''
    }
    
    this.route.params.subscribe(params => {
      if(params['id']) {
        this.isEdit = true;
        this.id = params['id'];
        this.homeService.lecturer(params['id']).subscribe(rs => {
          this.lecturer = rs.data;
          // this.graph.data.datasets[0].label = rs.role_id === 'COLLABORATOR' ? 'Income' : 'Outcome';
        });
      }
      
    });
  }

  onSubmit(): any {
    const lecturerSubmit: LecturerSubmit = this.lecturerForm.value;
    if(this.isEdit) {
      // let date_of_birth =  moment().format(lecturerSubmit.date_of_birth);
      // lecturerSubmit.date_of_birth = date_of_birth;
      this.homeService.updateLecturer(lecturerSubmit, this.id).subscribe((res) => {
        console.log(res);
        this.router.navigate(['pages/lecturer']);
      });
    }
    else {
      this.homeService.createLecturer(lecturerSubmit).subscribe((res) => {
        console.log(res);
        this.router.navigate(['pages/lecturer']);
      });
    } 

  }

  onCancel(): void {
    this.router.navigate(['pages/lecturer']);
  }

}
