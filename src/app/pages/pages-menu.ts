import { NbMenuItem } from '@nebular/theme';

export enum Role {
  ADMIN = 1,
  USER = 2
}

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Trang chủ',
    icon: 'home-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Quản lý tài khoản',
    icon: 'person-outline',
    link: '/pages/users',
  },
  {
    title: 'Quản lý sinh viên',
    icon: 'people-outline',
    link: '/pages/students',
  },
  {
    title: 'Quản lý tin tức',
    icon: 'layers-outline',
    link: '/pages/posts',
  },
  {
    title: 'Quản lý lớp',
    icon: 'award-outline',
    link: '/pages/class',
  },
  {
    title: 'Quản lý chung',
    icon: 'grid-outline',
    link: '/pages/master-manager',
  },
];

export const MENU_ITEMS_NV: NbMenuItem[] = [
  {
    title: 'Trang chủ',
    icon: 'home-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Quản lý sinh viên',
    icon: 'people-outline',
    link: '/pages/students',
  },
  {
    title: 'Quản lý tin tức',
    icon: 'layers-outline',
    link: '/pages/posts',
  },
  {
    title: 'Quản lý lớp',
    icon: 'award-outline',
    link: '/pages/class',
  },
  {
    title: 'Quản lý chung',
    icon: 'grid-outline',
    link: '/pages/master-manager',
  },
];
