import {Component, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user.model';
import {StatisticService} from '../../services/statistic.service';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';

@Component({
  selector: 'ngx-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
})
export class UserDetailsComponent implements OnInit {
  user: User;
  graph = {
    type: 'line',
    data: {
      labels: [],
      datasets: [
        {
          label: '',
          data: null,
          borderColor: 'rgb(75, 192, 192)',
          backgroundColor: 'rgba(75, 192, 192, 0.5)',
        },
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
    },
  };
  changUserStatusDialogRef: NbDialogRef<any>;

  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private statisticService: StatisticService,
              public dialogService: NbDialogService,
              private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.userService.one(params['id']).subscribe(rs => {
        this.user = rs;
        // this.graph.data.datasets[0].label = rs.role_id === 'COLLABORATOR' ? 'Income' : 'Outcome';
      });
    });
  }

  openChangeUserStatusDialog(ref: TemplateRef<any>): void {
    this.changUserStatusDialogRef = this.dialogService
      .open(ref, {context: this.user.status === 1 ? 'inactive' : 'active'});
  }

  changeUserStatus() {
    if (this.user.status === 1) {
      this.userService.inactiveUser(this.user.id)
        .subscribe(res => {
          this.user = res;
          this.toastrService.success('', 'Successfully');
          window.location.reload();
        }, err => this.toastrService.danger(err, 'Failed'));
    } else {
      this.userService.activeUser(this.user.id)
        .subscribe(res => {
          this.user = res;
          this.toastrService.success('', 'Successfully');
          window.location.reload();
        }, err => this.toastrService.danger(err, 'Failed'));
    }
    this.changUserStatusDialogRef.close();
  }
}
