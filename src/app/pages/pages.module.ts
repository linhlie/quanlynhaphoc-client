import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { UserComponent } from './user/user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { UserDetailsComponent } from './user-details/user-details.component';
import {ChartModule} from 'angular2-chartjs';
import { TopicComponent } from './topic/topic.component';
import { StudentsComponent } from './students/students.component';
import { ClassComponent } from './class/class.component';
import { PostComponent } from './post/post.component';
import { StudentDetailComponent } from './student-detail/student-detail.component';
// import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogChangeClassComponent } from './class/dialog-change-class/dialog-change-class.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MasterManagerComponent } from './master-manager/master-manager.component';
import { PaperSubmissionComponent } from './paper-submission/paper-submission.component';
import { UserCreateComponent } from './user-create/user-create.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbButtonGroupModule,
  NbButtonModule,
  NbCardModule,
  NbIconModule, 
  NbInputModule,
  NbMenuModule,
  NbSelectModule,
  NbTooltipModule,
} from '@nebular/theme';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser'
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { MasterCreateComponent } from './master-create/master-create.component';
import { LecturerComponent } from './lecturer/lecturer.component';
import { QuestionManagerComponent } from './question-manager/question-manager.component';
import { LecturerCreateComponent } from './lecturer-create/lecturer-create.component';
import { NotificationCreateComponent } from './notification-create/notification-create.component';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NotificationComponent } from './notification/notification.component';
import { ChatManagerComponent } from './chat-manager/chat-manager.component';
import { MatInputModule } from '@angular/material/input';
import { ChatRoomComponent } from './chat-room/chat-room.component';
import { RoomlistComponent } from './roomlist/roomlist.component';
import { AddroomComponent } from './addroom/addroom.component';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbCardModule,
    NbButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    NbSelectModule,
    NbButtonModule,
    NbIconModule,
    ChartModule,
    NbTooltipModule,
    FormsModule,
    NbInputModule,
    // BrowserModule,
    ReactiveFormsModule,
    NbLayoutModule,
    NbSidebarModule,
    CommonModule,
    HttpClientModule,
    // BrowserModule,
    NbCardModule,
    NbButtonGroupModule,
    NbButtonModule,
    NbEvaIconsModule,
    MatSidenavModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatSortModule,
    MatSnackBarModule,
    MatButtonModule
  ],
  declarations: [
    PagesComponent,
    UserComponent,
    DashboardComponent,
    NotFoundComponent,
    UserDetailsComponent,
    TopicComponent,
    StudentsComponent,
    ClassComponent,
    PostComponent,
    StudentDetailComponent,
    DialogChangeClassComponent,
    MasterManagerComponent,
    PaperSubmissionComponent,
    UserCreateComponent,
    MasterCreateComponent,
    LecturerComponent,
    QuestionManagerComponent,
    LecturerCreateComponent,
    NotificationCreateComponent,
    NotificationComponent,
    ChatManagerComponent,
    ChatRoomComponent,
    RoomlistComponent,
    AddroomComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class PagesModule {
}
