import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../services/home.service';
import { Lecturer } from '../../models/lecturer.model';
import {PageResponse} from '../../dto/page-response';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'ngx-lecturer',
  templateUrl: './lecturer.component.html',
  styleUrls: ['./lecturer.component.scss']
})

export class LecturerComponent implements OnInit {

  constructor( private homeService:HomeService) { }

  lecturer: PageResponse<Lecturer[]>;
  displayedColumns = ['id', 'name', 'email', 'address', 'phone', 'gender', 'status', 'actions'];

  ngOnInit(): void {
    this.homeService.listLecturer()
      .subscribe(rs => this.lecturer = rs);
  }

  pageChange($event: PageEvent) {
    this.homeService.listLecturer($event.pageIndex + 1, $event.pageSize)
      .subscribe(rs => this.lecturer = rs);
  }

  statusHighlight(status: number) {
    return status === 1 ? 'highlight-active' : 'highlight-inactive';
  }
}
