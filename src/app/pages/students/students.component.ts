import { Component, OnInit } from '@angular/core';
import {StudentService} from '../../services/student.service';
import {PageResponse} from '../../dto/page-response';
import {Student} from '../../models/student.model';
import {PageEvent} from '@angular/material/paginator';
import {PagesRoutingModule} from '../../pages/pages-routing.module';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { empty, Observable } from 'rxjs';
import { HomeService } from '../../services/home.service';
import { Statistic } from '../../models/statistic.model';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'ngx-student',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss'],
})
export class StudentsComponent implements OnInit {
  student: PageResponse<Student[]>;
  displayedColumns = ['id', 'full_name', 'code', 'id_card', 'exam_id', 'address', 'phone', 'status', 'date_of_birth', 'edit', 'paper'];

  constructor(private studentService: StudentService, private router:Router, private homeService:HomeService, private formBuilder: FormBuilder) { }

  statistics: Statistic
  status: any
  checkoutForm : FormGroup;
  statusSelect: 0;
  statusStudent: string;

  ngOnInit(): void {
    this.studentService.list()
      .subscribe(rs => this.student = rs);

    this.studentService.statistics()
      .subscribe(rs => this.statistics = rs);
      
    this.homeService.status()
      .subscribe(rs => this.status = rs.data);

      this.checkoutForm = this.formBuilder.group({
        code: '',
        industry_id: '',
      });
  }
  onCancelStatus(){
    this.statusStudent='';
    this.studentService.list()
      .subscribe(rs => this.student = rs);
  }

  pageChange($event: PageEvent) {
    if(!this.statusStudent) {
      this.studentService.listStatus($event.pageIndex + 1, $event.pageSize, this.statusStudent)
      .subscribe(rs => this.student = rs);
    } else {
      this.studentService.list($event.pageIndex + 1, $event.pageSize)
      .subscribe(rs => this.student = rs);
    }
    
  }

  roleHighlight(userRole: string) {
    switch (userRole) {
      case 'ADMIN': return 'highlight-admin';
      case 'VISITOR': return 'highlight-visitor';
      case 'COLLABORATOR': return 'highlight-collab';
    }
  }

  statusHighlight(status: number) {
    return status === 6 ? 'highlight-active' : 'highlight-inactive';
  }

  filterStudentStatus(value){
    this.statusStudent = value;
    this.studentService.listStatus(1,5,this.statusStudent)
      .subscribe(rs => this.student = rs);
    
  }

  gotoItems(code:string) {
    this.router.navigate(['/pages/paper', { code: code }]);
  }

  onSubmit(): any {
    const dataSubmit: any = this.checkoutForm.value;

    console.log("dataSubmit", dataSubmit)
    
      this.studentService.createCode(dataSubmit).subscribe((res) => {
        console.log(res);
        window.location.reload();
      });

  }

  onCancel(): void {
    this.router.navigate(['pages/question-manager']);
  }
}
