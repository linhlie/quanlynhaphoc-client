import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import jwt_decode from 'jwt-decode';

const TOKEN_KEY = '_token_030496';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  login(body: { email: string, password: string }): Observable<any> {
    const expire = new Date(new Date().getTime() + environment.tokenExpires);
    return this.http.post(`${environment.apiUrl}/auth/login`, body)
      .pipe(
        tap((res: any) => {
          this.cookieService.set(TOKEN_KEY, res.data.token, expire);
        }, err => {
          console.error(err);
        }),
      );
  }

  logout(): Observable<boolean> {
    this.cookieService.delete(TOKEN_KEY);
    this.cookieService.delete(TOKEN_KEY, '/');
    return this.isAuthorized().pipe(map(v => !v));
  }

  isAuthorized(): Observable<boolean> {
    return of(this.cookieService.check(TOKEN_KEY));
  }

  getToken(): string {
    return this.cookieService.get(TOKEN_KEY);
  }

  getPayload(): any {
    return jwt_decode(this.getToken());
  }
}
