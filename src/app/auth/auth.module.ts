import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { NbAlertModule, NbButtonModule, NbCheckboxModule, NbFormFieldModule, NbIconModule, NbInputModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { NbAuthModule } from '@nebular/auth';
// import { BrowserModule } from '@angular/platform-browser'


@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    NbIconModule,
    NbCheckboxModule,
    FormsModule,
    NbInputModule,
    NbAlertModule,
    NbButtonModule,
    NbAuthModule.forRoot({}),
    NbFormFieldModule,
    // BrowserModule
  ]
})
export class AuthModule { }
