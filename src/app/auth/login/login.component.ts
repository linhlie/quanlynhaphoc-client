import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { NbToastrService } from '@nebular/theme';
import firebase from "firebase";
import jwt_decode from 'jwt-decode';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {

  constructor(private auth: AuthService, private router: Router, private toastr: NbToastrService) {
  }

  user = {
    email: '',
    password: '',
  };

  ref = firebase.database().ref('users/');

  showPassword = false;

  login(): void {
    this.auth.login(this.user)
      .subscribe(res => {
        
        console.log("object", res);
        // const login = this.user;
        const login = this.auth.getPayload()

        this.ref.orderByChild('email').equalTo(login.email).once('value', snapshot => {
          if (snapshot.exists()) {
            localStorage.setItem('userLogin', JSON.stringify(login));
            this.router.navigate(['/']);
          } else {
            const newUser = firebase.database().ref('users/').push();
            newUser.set(login);
            localStorage.setItem('userLogin', JSON.stringify(login));
            this.router.navigate(['/']);
          }
        });
      }, err => {
        this.toastr.danger('Login failed', 'Oops');
      });
  }

  getInputType(): string {
    if (this.showPassword) {
      return 'text';
    }
    return 'password';
  }

  toggleShowPassword(): void {
    this.showPassword = !this.showPassword;
  }
}

