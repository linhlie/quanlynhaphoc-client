import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpHeaders, HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {AuthService} from './auth.service';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  authErrCodes = [401, 403];

  constructor(private auth: AuthService,
              private router: Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    request = request.clone({
      headers: new HttpHeaders({
        Authorization:  `Bearer ${this.auth.getToken()}`,
        
        'Access-Control-Allow-Origin': '*',
      }),
    });
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (this.authErrCodes.includes(error.status)) {
          this.auth.logout().subscribe(res => {
            if (res) {
              this.router.navigate(['auth/login']);
            }
          });
        }
        return throwError(error);
      }),
    );
  }
}
