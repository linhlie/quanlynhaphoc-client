/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  apiUrl: 'http://doan.aicdemo.com/api',
  // apiUrl: 'http://127.0.0.1:8000/api',
};
